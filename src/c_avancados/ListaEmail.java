package c_avancados;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ListaEmail {
	public static void main(String[] args)
	{
		//ArrayList<String> lista = new ArrayList<>();
		Path pathr = Paths.get("bin/contas.csv");
		Path pathw = Paths.get("contas.txt");

		try {
			List<String> lista = Files.readAllLines(pathr);
			lista.remove(0);
			String email = "";
			for(String linha: lista) {
				String[] partes = linha.split(",");
				if (Double.parseDouble(partes[4]) > 7000) {
					email += String.format("%s %s <%s>, \n", partes[1], partes[2], partes[3]);
				}			
			}
			Files.write(pathw, email.getBytes());
		}
		catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
}